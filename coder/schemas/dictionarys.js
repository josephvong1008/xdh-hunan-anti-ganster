module.exports = {
  vuex: true,
  model: [
    {
      'title': '获取字典项',
      'path': '/shry/Code',
      'methods': false,
      // 'method': 'get',
      'options': {
        'method': 'post'
      },
      'name': 'getDicOptions',
      'state': 'dicOptions',
      'template': 'getDicOptions'
      // 'state': 'concernPersonList',
      // 'template': 'getConcernPersons' // mock/template.js里的getAdminAccusations
    }
  ]
}