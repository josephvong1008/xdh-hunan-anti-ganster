
module.exports = {
  // 站点名称标识
  name: 'bigscreen',
  
  // 部署访问目录
  baseUrl: '/xdh-hunan-anti-ganster/', // '/',
  
  // 开发环境启动服务端口号
  port: 8001,
  
  // 打包发布目录
  outDir: '/',
  
  // 扩展的静态资源目录
  extendContentBase: []
}
