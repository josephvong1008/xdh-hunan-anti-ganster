module.exports = {
  vuex: true,
  model: [
    {
      'title': '登录验证',
      'path': '/user/login',
      'methods': false,
      // 'method': 'get',
      'options': {
        'method': 'get'
      },
      'name': 'login',
      'state': 'loginInfo',
      'template': 'loginMock'
      // 'state': 'concernPersonList',
      // 'template': 'getConcernPersons' // mock/template.js里的getAdminAccusations
    }
  ]
}