const _import = require('../utils/view-import/' + process.env.NODE_ENV)

export default {
  routes: [
    {
      path: '/',
      redirect: '/index'
    },
    {
      path: '/',
      component: _import('index'),
      children: [
        {
          path: '/index',
          component: _import('curd'),
          name: '涉黑人员管理'
        },
        {
          path: '/case-detective',
          component: _import('prepare'),
          name: '案件研判'
        },
        {
          path: '/curd-manage',
          component: _import('prepare'),
          name: '线索管理'
        },

        {
          path: '/order-verify',
          component: _import('prepare'),
          name: '指令核查'
        },

        {
          path: '/warning',
          component: _import('prepare'),
          name: '预警研判'
        },

        {
          path: '/system-tool',
          component: _import('prepare'),
          name: '系统工具'
        }
      ]
    },
    {
      path: '/person/:rybh',
      component: _import('person'),
      name: '人员信息'
    },
    {
      path: '/403',
      component: _import('403')
    },
    {
      path: '*',
      component: _import('404')
    }
  ]
}
