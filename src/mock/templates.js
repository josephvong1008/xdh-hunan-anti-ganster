
/**
 * 以函数的方式自定义模拟数据模板
 *
 * @module mock/templates
 *
 * @example 模板定义方法
 *
 * export function templateFuncName(Mock, url, query) {
 *    return {code:0, data:[], msg: ''}
 * }
 *
 */
import {parse} from '../utils/url'
import Mock from 'mockjs'
const Random = Mock.Random
// { prop: 'RYBH', label: '人员编号' },
//   { prop: 'XM', label: '姓名' },
//   { prop: 'SEX', label: '性别' },
//   { prop: 'SFZH', label: '身份证号' },
//   { prop: 'SHSEGZRY_PDBZ', label: '是否涉及公职人员' },
//   { prop: 'SJAJ_JYQK', label: '涉案信息' }
Random.extend({
  concernPersons: function(size) {
    
    let temp = []
    for (let index = 0; index < size; index++) {
      let obj = {
        'rybh': Random.id(), 
        'xm': Random.cname(),
        'sex': Random.boolean() ? '男' : '女',
        'sfzh': Random.id(),
        'shsegzryPdbz': Random.boolean() ? '是' : '否',
        'sjajJyqk': Random.csentence(),
        'ROW_ID': index
      }
      temp.push(obj)
    }
    
    return temp
  }
})

const getConcernPersonsMock = function(Mock, url, query) {
  return Mock.mock({
    code: 0,
    msg: '',
    data: {
      page: query.pageIndex,
      limit: query.pageSize,
      total: 200,
      list: `@concernPersons(${query.pageSize})`
    }
  })
}

export function getConcernPersons(Mock, url, query) {
  return getConcernPersonsMock(Mock, url, query)
}

export function loginMock(Mock, url, query) {
  console.log('mock', url)
  return Mock.mock({
    code: 0,
    msg: '',
    data: {
      code: parse(url.split('?')[1]).sfzh === '440104' ? 0 : 500,
      msg: parse(url.split('?')[1]).sfzh === '440104' ? '登录成功' : '登录失败'
    }
  })
}

export function saveConcernPerson(Mock, url, query) {
  return Mock.mock({
    code: 0,
    msg: '',
    data: {
      submit: query
    }
  })
}

export function getDicOptions(Mock, url, query) {
  return Mock.mock({
    code: 0,
    msg: '',
    data: [
      {
        'CODE': '1',
        'BZ': '涉黑活动地域代码',
        'TYPE': 'S001',
        'NAME': '农村'
      },
      {
        'CODE': '3',
        'BZ': '涉黑活动地域代码',
        'TYPE': 'S001',
        'NAME': '城乡结合部'
      },
      {
        'CODE': '4',
        'BZ': '涉黑活动地域代码',
        'TYPE': 'S001',
        'NAME': '城市'
      }
    ]
  })
}

export function checkIsExists(Mock, url, query) {
  return Mock.mock({
    code: 0,
    msg: query.sfzh === '440104' ? '用户已经存在了' : '添加成功',
    data: query.sfzh === '440104'
  })
}

export function selectOne(Mock, url, query) {
  return Mock.mock({
    code: 0,
    msg: '',
    data: {
      xm: '黄吉平', // 姓名
      sex: '1', // 性别代码
      zw: '1323', // 职务
      sfzh: '12345678522', // 身份证号

      zzsfdm: '1', // 政治身份代码
      shsegzryPdbz: '2', // 是否涉及公职人员
      csrq: '2', // 出生日期
      gjdm: '1', // 国籍代码

      mzdm: '1', // 民族代码
      hjdzDzmc: '2', // 户籍地址_地址名称

      xzzXzqhdm: '1', // 现住址_行政区划代码
      xzzDzmc: '2', // 现住址_地址名称
      jwzzGjhdqdm: '1', // 境外住址_国家和地区代
      jwzzDzmc: '2', // 境外住址_地址名称

      bmch: '1', // 别名/绰号
      zzmmdm: '2', // 政治面貌代码
      jyzkdm: '1', // 婚姻状况代码
      xldm: '2', // 学历代码

      laSfsqPdbz: '1', // 立案_是否涉枪
      laSfsbPdbz: '2', // 立案_是否涉爆
      laSfmaPdbz: '2', // 立案_是否命案
      laSfswPdbz: '1', // 立案_是否涉外

      sfjsbrPdbz: '1', // 是否精神病人_判断标识
      sg: '2', // 身高
      tz: '2', // 体重
      zc: '1', // 足长

      tmtzms: '2', // 体貌特征描述
      tbbjms: '1', // 体表标记描述

      bz: '阿斯顿发斯蒂芬撒地方', // 备注
      sjajJyqk: '阿斯顿发送到发送到发送到飞洒地方' // 涉案信息   
    }
  })
}