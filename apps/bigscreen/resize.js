export function winResize(minWidth = 1280) {
  function resize() {
    let w = window.document.documentElement.clientWidth,
      h = window.document.documentElement.clientHeight;
    if (w > minWidth) {
      document.body.style = '';
      return;
    }
    let p = h / w;
    let width = minWidth,
      height = width * p;
    let sx = w / width,
      sy = h / height;
    document.body.style = `width: ${width}px;height:${height}px;transform: scale(${sx}, ${sy});transform-origin: top left`;
  }
  window.addEventListener('resize', function(e) {
    resize();
  });
  resize();
}