module.exports = {
  vuex: true,
  model: [
    {
      'title': '关注人员目标搜索',
      'path': '/shry/select',
      'methods': false,
      'method': 'fetch',
      'name': 'getConcernPersons',
      'state': 'concernPersonList',
      'template': 'getConcernPersons' // mock/template.js里的getAdminAccusations
    },
    {
      'title': '关注人员基本信息保存',
      'path': '/shry/save',
      'methods': false,
      'method': 'add',
      'name': 'saveConcernPerson',
      // 'state': 'concernPersonList',
      'template': 'saveConcernPerson' // mock/template.js里的getAdminAccusations
    },
    {
      'title': '验证身份证是否存在',
      'path': '/shry/checkIsExists',
      'methods': false,
      'options': {
        'method': 'get'
      },
      'name': 'checkIsExists',
      'state': 'isExists',
      'template': 'checkIsExists' // mock/template.js里的getAdminAccusations
    },

    {
      'title': '获取单个人员',
      'path': '/shry/selectOne',
      'methods': false,
      'options': {
        'method': 'get'
      },
      'name': 'selectOne',
      'state': 'targetPerson',
      'template': 'selectOne' // mock/template.js里的getAdminAccusations
    }
  ]
}