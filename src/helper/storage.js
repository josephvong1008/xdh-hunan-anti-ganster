
import { save, get } from '../utils/storage';
const key = '__SFZH__'

let SFZH = ''

export const setSFZH = function(sfzh) {
  save(key, sfzh)
  SFZH = sfzh
}

export const getSFZH = function() {
  if (!SFZH) {
    SFZH = get(key) || ''
  }
  return SFZH
}