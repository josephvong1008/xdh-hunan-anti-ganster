/**
 * 路由导航守卫
 * @function
 * @param {VueRouter} router 路由实例
 */

import NProgress from 'nprogress'
import store from '../store'
import {parse} from '../utils/url'
import { setSFZH, getSFZH } from './storage';
// 进度自动递增20%
NProgress.inc(0.2)

const defineSFZH = function(sfzh) { // , to, next
  return store.dispatch('LOGIN', {data: {sfzh: sfzh}}).then((res) => {
    if (res.code === 0) {
      store.commit('setIsLogin', true)
      setSFZH(sfzh)
      return Promise.resolve()
    } else {
      let err = new Error()
      return Promise.reject(err)
    }
  }).catch((e) => {  
    console.log(e, '接口失败 + 登录失败')
    let err = new Error()
    return Promise.reject(err)
  })
}

export default function (router) {
  /**
   * 全局前置守卫
   */
  router.beforeEach((to, from, next) => {
    NProgress.start() 
    if (to.path !== '/403') {
      if (!store.state.isLogin) {
        let sfzh = parse(window.location.href.split('?')[1]).SFZH
        if (sfzh) { // url有sfzh
          defineSFZH(sfzh).then(() => {
            let obj = Object.assign({}, to, {query: {}})
            next(obj)
          }).catch(() => {
            next({path: '/403'})
            NProgress.done()
          })
        } else {
          let oldsfzh = getSFZH() // 取storage 中的sfzh
          if (oldsfzh) {
            defineSFZH(oldsfzh).then(() => {
              next()
            }).catch(() => {
              next({path: '/403'})
              NProgress.done()
            }) // 身份证号去验证
          } else {
            console.log('没有验证信息')
            next({path: '/403'})
            NProgress.done()
          }
        }
      } else {
        next()
      }
    } else {
      if (store.state.isLogin) {
        next({path: '/index'})
      } else {
        console.log('就是去403')
        next()
      }
      
    }
  });
  
  /**
   * 全局后置守卫
   */
  router.afterEach((to, from) => {
    NProgress.done()
  })
  
}


