const _import = require('./view-import/' + process.env.NODE_ENV)
export default {
  routes: [
    {
      path: '/',
      component: _import('index')
    },
    {
      path: '/test',
      component: _import('test')
    },
    {
      path: '/test2',
      component: _import('test2')
    },
    {
      path: '/test3',
      component: _import('test3')
    },
    {
      path: '/test4',
      component: _import('test4')
    },
    {
      path: '/test5',
      component: _import('test5')
    }
  ]
}
